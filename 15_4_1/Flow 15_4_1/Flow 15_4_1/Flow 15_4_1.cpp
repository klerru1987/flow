﻿#include <iostream>

using namespace std;

int main()
{
    setlocale(LC_ALL, "RU");

    int N = 15;

    for (int i = 1; i <= N; i++)
    {
        if (i % 2 == 0)
        {
            cout << i << endl;
        }
    }

    return 0;
}

