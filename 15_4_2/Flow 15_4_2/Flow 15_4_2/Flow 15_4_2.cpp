﻿#include <iostream>

using namespace std;

void MyFunction(int N, int b)
{
    for (int i = 1; i <= N; i++)
    {
        if (i % 2 == b)
        {
            cout << i << endl;
        }
    }
}

int main()
{
    setlocale(LC_ALL, "RU");
    int N, b;
    cout << "Введите число " << endl;
    cin >> N;
    cout << "Введите 0, чтобы вывести четные значения. Введите 1, чтобы вывести не четные значения " << endl;
    cin >> b;
    cout << "Полученные значения:" << endl;
    MyFunction(N, b);

    return 0;
}

